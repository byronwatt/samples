With old freertos labs it didn't work:

```plantuml
@startuml
actor hostmsg
actor "thread_a\nhigh priority" as high_priority_thread
actor "thread_b\nlow priority" as low_priority_thread
control gdma
activate hostmsg #FBB4AE
hostmsg -> gdma ++ #FBB4AE : acquire ch 1
return ch1 granted
hostmsg -> gdma ++ #FBB4AE : acquire ch 2
return ch2 granted
hostmsg -> high_priority_thread ++ #FED9A6 : start thread a (high priority)
hostmsg -> low_priority_thread ++ #CCEBC5 : start thread b (low priority)
hostmsg -> hostmsg : sleep a smidge
high_priority_thread -> gdma ++ #FED9A6 : acquire ch 1
low_priority_thread -> gdma ++ #CCEBC5 : acquire ch 2

hostmsg -> gdma : release ch 2
gdma -> high_priority_thread : retest if ch 1 is available (not)
gdma -> high_priority_thread : retest if ch 1 is available (not)
... low priority thread never awakened to retest if ch2 available ...
hostmsg -> low_priority_thread : wait for low priority thread to finish
hostmsg -> gdma : release ch 1
hostmsg -> hostmsg : wait for high priority thread to finish
hide footbox
@enduml
```

With current code it should work properly:

```plantuml
@startuml
actor hostmsg
actor "thread_a\nhigh priority" as high_priority_thread
actor "thread_b\nlow priority" as low_priority_thread
control gdma
activate hostmsg #FBB4AE
hostmsg -> gdma ++ #FBB4AE : acquire ch 1
return ch1 granted
hostmsg -> gdma ++ #FBB4AE : acquire ch 2
return ch2 granted
hostmsg -> high_priority_thread ++ #FED9A6 : start thread a (high priority)
hostmsg -> low_priority_thread ++ #CCEBC5 : start thread b (low priority)
hostmsg -> hostmsg : sleep a smidge
high_priority_thread -> gdma ++ #FED9A6 : acquire ch 1
low_priority_thread -> gdma ++ #CCEBC5 : acquire ch 2

hostmsg -> gdma : release ch 2
gdma -> high_priority_thread : retest if ch 1 is available (not)
gdma -> low_priority_thread -- : retest if ch 2 is available (yes!)
hostmsg -> low_priority_thread : wait for low priority thread to finish
low_priority_thread -> hostmsg -- : finished
hostmsg -> gdma : release ch 1
gdma -> high_priority_thread -- : retest if ch 1 is available (yes!)
hostmsg -> high_priority_thread : wait for high priority thread to finish
high_priority_thread -> hostmsg -- : finished
hide footbox
@enduml
```

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).